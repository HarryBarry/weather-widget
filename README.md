# Weather Widget (Isolated React JS components)
**Easy Start**
**Just import widget reducer to (APP/reducers/index.js), and import Widget Component.**
Working on Openweather API (http://openweathermap.org).

# INSTALL PACKAGES
npm i

# TO RUN DEV
npm dev

# TO RUN PROD
npm build
npm server


# TO DO
- ~~weather forecast~~ (~~today~~, ~~week~~, ~~2 weeks, etc~~)
- ~~changing unit types~~
- favorite list (with likes)
- recent list
- add graphics
- add service worker
